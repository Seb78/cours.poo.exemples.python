import unittest
from coo import point2d

class PointTestCase(unittest.TestCase):
    def test_point_creation(self):
        p = point2d.Point2D(1.0, 2.0)
        self.assertEqual(p.x, 1.0)
        self.assertEqual(p.y, 2.0)

if __name__ == '__main__':
    unittest.main()
