import unittest
from coo import cercle2d, point2d

class CercleTestCase(unittest.TestCase):
    def test_cercle_creation(self):
        centre = point2d.Point2D(1.0, 2.0)
        c = cercle2d.Cercle2D(centre, 3.0)
        self.assertEqual(c.centre, centre)
        self.assertEqual(c.rayon, 3.0)

if __name__ == '__main__':
    unittest.main()
